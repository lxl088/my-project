package com.fulan.config;

import com.baomidou.mybatisplus.core.incrementer.IKeyGenerator;
import com.baomidou.mybatisplus.extension.incrementer.DB2KeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author lvxingli
 * @Date 2021-10-26 13:50
 **/
@Configuration
public class IdAutoConfig {
    @Bean
    public IKeyGenerator keyGenerator(){
        return new DB2KeyGenerator();
    }
}
