package com.fulan.pojo;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

@Data
//指定序列来实现自增 此时的@TableId的type设置为IdType.Input
//@KeySequence(value = "keySequence",clazz = Integer.class)
public class User {
    //设置主键自增，前提是数据库字段已经设置为自增了
//    @TableId(value = "id",type = IdType.AUTO)
//    @TableId(value = "id",type = IdType.INPUT)
//    @TableId(value = "id",type = IdType.UUID)
    @TableId(value = "id")
    private Long id;
    @TableField(value = "name")
    private String name;
    @TableField(value = "age")
    private Integer age;
    @TableField(value = "email")
    private String email;
    @TableField(value = "deleted")
    @TableLogic(value = "0",delval = "1")
    private int deleted;
    @TableField(value = "version")
    @Version
    private int version;

}