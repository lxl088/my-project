package com.fulan.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fulan.pojo.User;
import org.apache.ibatis.annotations.Mapper;

import javax.annotation.Resource;

/**
 * @Author lvxingli
 * @Date 2021-10-26 11:01
 * 用户表数据访问层接口
 **/
public interface UserMapper extends BaseMapper<User> {
}
