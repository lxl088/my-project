package com.fulan;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fulan.mapper.UserMapper;
import com.fulan.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
class MybatisPlusDemo1ApplicationTests {

    @Test
    void contextLoads() {
    }
    @Autowired
    UserMapper userMapper;

    /**
     * 查询全部记录
     */
    @Test
    void select(){
        List<User> list=userMapper.selectList(null);
        System.out.println(list);
    }

    /**
     * 插入一条记录
     */
    @Test
    void insert(){
        User user=new User();
        user.setId(6L);
        user.setAge(18);
        user.setName("小明");
        user.setEmail("20880@qq.com");
        int i = userMapper.insert(user);
        System.out.println(i);
    }

    /**
     * 根据条件来删除一条记录
     */
    @Test
    void delete(){
        //创建queryWrapper对象
        QueryWrapper<User> userWrapper = new QueryWrapper<>();
        //约束条件 支持链式编程
        userWrapper.lt("id","1");
        userMapper.delete(userWrapper);
    }

    /**
     * 根据queryWrapper条件进行查询
     */
    @Test
    void query(){
        //根据id查询一条记录
        System.out.println(userMapper.selectById("2"));
        //根据条件查询一条记录
        QueryWrapper queryWrapper=new QueryWrapper();
        queryWrapper.eq("id",4);
        System.out.println(userMapper.selectOne(queryWrapper));
        //根据ids查询多条记录
        System.out.println("------------");
        System.out.println(userMapper.selectBatchIds(Arrays.asList(1, 2, 3, 4)));
        //根据查询条件查询多条记录
        System.out.println("-------------");
        QueryWrapper queryWrapper1=new QueryWrapper();
        queryWrapper1.le("id","5");
        System.out.println(userMapper.selectList(queryWrapper1));
        //指定map（字段，值）来查询多条记录
        System.out.println("-----------------");
        Map<String,Object> map=new HashMap<>();
        map.put("name","小明");
        System.out.println(userMapper.selectByMap(map));
        //根据条件进行分页查询
        System.out.println("--------分页查询---------");
        QueryWrapper queryWrapper2=new QueryWrapper();
        queryWrapper2.orderByDesc("id");
        Page<User> page=new Page<>();
        page.setCurrent(2L);
        page.setSize(2L);
        IPage selectPage = userMapper.selectPage(page, queryWrapper2);
        selectPage.getPages();
        List<User> records = selectPage.getRecords();
        System.out.println(records);
        //根据查询条件查询记录数
        System.out.println("----------根据查询条件查询记录数---------");
        QueryWrapper<User> queryWrapper3=new QueryWrapper<>();
        queryWrapper3.le("id",4);
        System.out.println(userMapper.selectCount(queryWrapper3));
    }

    /**
     * 测试主键自增
     * 第一种方式：主键字段添加 @TableId(value = "id",type = IdType.AUTO)顾名思义自增，并且设置数据库字段为自增（适用于mysql），其实这种方式
     * 的主要功能体现在数据库上的字段自增上，该注解的主要功能就在于设置为IdType.AUTO之后，该主键字段会回显注入
     * 第二种方式：不需要指定数据库字段自增，主键字段添加@TableId(value = "id",type = IdType.INPUT)顾名思义 输入，并且在该实体类上添加@KeySequence(value = "keySequence",clazz = Integer.class)注解
     * 该注解的参数分别是序列的名字，以及序列产生的值的类型（未测试）一般用于oracle
     * 第三种方式：uuid，主键字段添加 该@TableId(value = "id",type = IdType.UUID)注解，然后数据库不需要设置子段自增，但是由于uuid是字符串，所以该字段不能是int类型
     */
    @Test
    void insertAuto(){
        System.out.println("---------测试主键自增--------------");
        User user=new User();
        user.setEmail("20000@qq.com");
        user.setAge(28);
        user.setName("小红");
        System.out.println(userMapper.insert(user));
        //主键自增之后返回注入给实体类对应的主键字段
        System.out.println(user);
    }

    /**
     * 测试逻辑删除
     * 1、配置application.properties对应的配置
     * 2、在实体类对应的字段上添加@deleted注解（不一定非要是version字段，但是字段类型只能是 int、Date）
     */
    @Test
    void deleteLujiDelete(){
        System.out.println("测试逻辑删除");
        System.out.println("执行逻辑删除");
        System.out.println(userMapper.deleteById(3L));
        System.out.println("查询逻辑删除之后的记录");
        //通过id查询不可以查询出来
        System.out.println(userMapper.selectById(3L));
        QueryWrapper<User> queryWrapper=new QueryWrapper<>();
        //通过查询条件同样不可以查询出来
        queryWrapper.eq("age",28);
        System.out.println(userMapper.selectOne(queryWrapper));
    }
    /**
     * 测试乐观锁
     * 1、配置乐观锁配置类
     * 2、在version实体类字段上添加@version注解
     */
    @Test
    void updateForLock(){
        User user=new User();
        user.setName("lilei");
        user.setAge(19);
        // version字段不指定值时，更新匹配条件的记录，并使得version加1，
        // 但是如果version指定值，那么它会将version字段当作匹配条件，此时只会更新version为该值的记录
        user.setVersion(3);
        user.setId(4L);
        System.out.println(userMapper.updateById(user));
    }


}
