package cn.es.controller;

import cn.es.util.EsUtil;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * elasticSearch的控制器
 */
@RestController
public class EsController {
    @Autowired
    RestHighLevelClient client;
    String index="jd_index";
    @GetMapping("/insertEs/{key}")
    public Boolean insertEs(@PathVariable String key){
        Boolean flag=false;
        try {
            flag= EsUtil.insertListEs(client,key,index);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return flag;
    }
    @GetMapping("/searchEs/{key}/{pageNum}/{pageSize}")
    public List<Map<String,Object>> searchEs(@PathVariable String key, @PathVariable int pageNum, @PathVariable int pageSize) throws IOException {
        List<Map<String,Object>> list=EsUtil.searchEs(client,index,key,pageNum,pageSize);
        //返回结果
        return list;
    }
    @GetMapping("/searchEsHighlight/{key}/{pageNum}/{pageSize}")
    public List<Map<String,Object>> searchEsHighlight(@PathVariable String key, @PathVariable int pageNum, @PathVariable int pageSize) throws IOException {
        List<Map<String,Object>> list=EsUtil.searchEsHighlight(client,index,key,pageNum,pageSize);
        //返回结果
        return list;
    }

}
