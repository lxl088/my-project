package cn.es;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
@ServletComponentScan
public class ElasticSearchJdApplication {

    public static void main(String[] args) {
        SpringApplication.run(ElasticSearchJdApplication.class, args);
    }

}
