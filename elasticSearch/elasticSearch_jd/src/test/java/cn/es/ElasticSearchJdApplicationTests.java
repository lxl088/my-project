package cn.es;

import cn.es.pojo.Content;
import cn.es.util.EsUtil;
import cn.es.util.HtmlParseUtil;
import com.alibaba.fastjson.JSON;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@SpringBootTest
class ElasticSearchJdApplicationTests {

    @Test
    void contextLoads() {
        try {
            EsUtil.insertListEs(restHighLevelClient,"华为","jd_index");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Autowired
    RestHighLevelClient restHighLevelClient;
    @Test
    void test1() throws IOException {
            List<Map<String,Object>> list=new ArrayList<>();
            //1、条件搜索 参数 索引
            SearchRequest searchRequest=new SearchRequest("jd_index");
            //2、构建搜索条件
            SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
            //3、分页
            searchSourceBuilder.from(10);
            searchSourceBuilder.size(10);

            //4、查询条件 精确匹配
            TermQueryBuilder termQueryBuilder = QueryBuilders.termQuery("title", "华为");
            //5、注入执行查询条件
            searchSourceBuilder.query(termQueryBuilder);
            //6、设置查询超时时间
            searchSourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));

            //7、执行查询 返回结果
            searchRequest.source(searchSourceBuilder);
            SearchResponse response = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
            System.out.println(response);
            for (SearchHit hit : response.getHits().getHits()) {
                //遍历查询结果
                System.out.println(hit.getSourceAsMap());
                Map<String, Object> map = hit.getSourceAsMap();
                System.out.println(map);
                list.add(map);
            }


    }

}
