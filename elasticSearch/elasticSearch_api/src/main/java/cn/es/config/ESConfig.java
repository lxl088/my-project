package cn.es.config;

import org.apache.http.HttpHost;
import org.elasticsearch.client.Node;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * elasticSearch配置
 */
//类似于spring 的xml
@Configuration
public class ESConfig {
    /*<bean id=restHighLevelClient class="org.elasticsearch.client.RestHighLevelClient">*/
    //注入一个高级客户端
    @Bean
    public RestHighLevelClient restHighLevelClient(){
        RestHighLevelClient restHighLevelClient=new RestHighLevelClient(RestClient.builder(new HttpHost("localhost",9200,"http")));
        return  restHighLevelClient;
    }

}
