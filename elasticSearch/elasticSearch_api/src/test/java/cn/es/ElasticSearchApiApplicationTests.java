package cn.es;

import cn.es.pojo.User;
import com.alibaba.fastjson.JSON;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.FetchSourceContext;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * 索引 可以看做 “数据库”
 * 类型 可以看做 “表”
 * 文档 可以看做 “库中的数据（表中的行）”
 */
@SpringBootTest
class ElasticSearchApiApplicationTests {
    @Autowired
    RestHighLevelClient client;

    @Test
    void contextLoads() {
    }
    //一、创建索引
    @Test
    void createIndex() throws IOException {
        //1/创建新建索引（库） 的请求
        CreateIndexRequest createIndexRequest= new CreateIndexRequest("lxl_demo");
//        createIndexRequest
        //2、执行请求，获得响应
        CreateIndexResponse createIndexResponse= client.indices().create(createIndexRequest, RequestOptions.DEFAULT);
        System.out.println(createIndexResponse);
    }
    //二、获取索引
    @Test
    void getIndex() throws IOException {
        //1/创建新建索引（库） 的请求
        GetIndexRequest request= new GetIndexRequest("lxl_demo");
        //2、判断该索引是否存在
        boolean flag_exist= client.indices().exists(request,RequestOptions.DEFAULT);
        System.out.println(flag_exist);
    }
    //三、删除索引
    @Test
    void deleteIndex() throws IOException {
        //1/创建新建索引（库） 的请求
        DeleteIndexRequest request= new DeleteIndexRequest("lxl_demo");
        //2、判断该索引是否存在
        AcknowledgedResponse response= client.indices().delete(request,RequestOptions.DEFAULT);
        System.out.println(response.isAcknowledged());
    }

    //四、插入文档 （表中的行） 第一次插入，第二次覆盖更新
    @Test
    void addDocument() throws IOException {
        User user=new User(1,"小明",18);
        //1、创建请求
        IndexRequest indexRequest= new IndexRequest("lxl_demo");
        //3、规则
        indexRequest.id(String.valueOf(user.getId()));
        indexRequest.timeout(TimeValue.timeValueSeconds(1));
        indexRequest.timeout("1s");
        //将我们数据放入请求 json格式
        indexRequest.source(JSON.toJSONString(user), XContentType.JSON);
        //客户端发送请求
        IndexResponse response= client.index(indexRequest,RequestOptions.DEFAULT);
        System.out.println(response.toString());
        System.out.println(response.status());
    }
    //获取文档 判断文档是否存在
    @Test
    void existDocument() throws IOException {
        GetRequest getRequest=new GetRequest("lxl_demo","1");
        //不返回source上下文
        getRequest.fetchSourceContext(new FetchSourceContext(false));
        getRequest.storedFields("_none_");
        boolean flag=client.existsSource(getRequest,RequestOptions.DEFAULT);
        System.out.println(flag);
    }
    //获取文档 获取文档内容
    @Test
    void getDocument() throws IOException {
        GetRequest getRequest=new GetRequest("lxl_demo","1");
        GetResponse getResponse =client.get(getRequest,RequestOptions.DEFAULT);
        System.out.println(getResponse.toString());
        System.out.println(getResponse.getSourceAsString());//打印文档的内容
    }
    //更新文档记录 更新文档内容 空属性值保留原值，非空属性值更新
    @Test
    void updateDocument() throws IOException {
        User user=new User(1,null,81);

        UpdateRequest request=new UpdateRequest("lxl_demo","1");
        request.timeout("1s");
        //将我们数据放入请求 json格式
        request.doc(JSON.toJSONString(user), XContentType.JSON);
        UpdateResponse updateResponse =client.update(request,RequestOptions.DEFAULT);
        System.out.println(updateResponse.toString());
    }
    //更新文档 更新文档内容 空属性值保留原值，非空属性值更新
    @Test
    void deleteDocument() throws IOException {
        User user=new User(1,null,81);

        DeleteRequest request=new DeleteRequest("lxl_demo","1");
        request.timeout("1s");
        DeleteResponse deleteResponse =client.delete(request,RequestOptions.DEFAULT);
        System.out.println(deleteResponse.toString());
    }
    //真实项目大批量的插入数据
    @Test
    void insertBulkRequest() throws IOException {
        BulkRequest request=new BulkRequest();
        request.timeout("10s");
        ArrayList<User> arrayList=new ArrayList<>();
        arrayList.add(new User(10,"zs",18));
        arrayList.add(new User(11,"lisi",21));
        arrayList.add(new User(12,"ww",16));
        arrayList.add(new User(13,"zl",15));
        arrayList.add(new User(14,"hh",16));
        arrayList.add(new User(15,"ll",14));
        for (User user : arrayList) {
            request.add(
                    new IndexRequest("lxl_demo")
                    .id(String.valueOf(user.getId()))
                            .source(JSON.toJSONString(user),XContentType.JSON));
        }
        BulkResponse response= client.bulk(request,RequestOptions.DEFAULT);
        System.out.println(response.hasFailures());
    }

    /**
     * 查询
     * SearchRequest 搜索请求
     * SearchSourceBuilder 条件构造
     *
     * @throws IOException
     */
    @Test
    //查询
    void searchRequest() throws IOException {
        SearchRequest searchRequest=new SearchRequest();
        //构建搜索条件
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        //查询条件 精确匹配
        TermQueryBuilder termQueryBuilder = QueryBuilders.termQuery("name", "zs");

        searchSourceBuilder.query(termQueryBuilder);
        searchSourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));

        searchRequest.source(searchSourceBuilder);
        //执行查询 返回结果
        SearchResponse response = client.search(searchRequest, RequestOptions.DEFAULT);

        System.out.println(JSON.toJSONString(response.getHits()));
        System.out.println("-----------------------");
        for (SearchHit hit : response.getHits().getHits()) {
            System.out.println(hit.getSourceAsMap());
        }
    }
    

}