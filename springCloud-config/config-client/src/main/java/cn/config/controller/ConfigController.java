package cn.config.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RefreshScope
public class ConfigController {
    @Value("${config-file}")
    String config_msg;
    @GetMapping("/getConfig")
    public String getConfig(){
        return config_msg;
    }
}
