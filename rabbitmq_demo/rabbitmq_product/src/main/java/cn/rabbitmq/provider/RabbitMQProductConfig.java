package cn.rabbitmq.provider;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQProductConfig {
    //交换机名称
    public static final String Blog_EXCHANGE="blog_exchange";

    //产品队列名称
    public static final String PRODUCT_QUEUE="product_queue";
    //订单队列名称
    public static final String ORDER_QUEUE="order_queue";

    /*声明blog交换机*/
    @Bean("blogExchange")
    public Exchange blog_exchange(){
        //类型为通配模式 持久化
        return ExchangeBuilder.topicExchange(Blog_EXCHANGE).durable(true).build();
    }
    /*声明产品队列*/
    @Bean("productQueue")
    public Queue productQueue(){
        return QueueBuilder.durable(PRODUCT_QUEUE).build();
    }
    /*声明订单队列*/
    @Bean("orderQueue")
    public Queue orderQueue(){
        return QueueBuilder.durable(ORDER_QUEUE).build();
    }
    /*绑定产品队列到blog交换机上*/

    @Bean
    public Binding productQueueBlogExchangeBind(@Qualifier("blogExchange") Exchange exchange,@Qualifier("productQueue") Queue queue){

        return BindingBuilder.bind(queue).to(exchange).with("product.*").noargs();
    }
    /*绑定订单队列到blog交换机上*/

    @Bean
    public Binding orderQueueEbuyExchangeBind(@Qualifier("blogExchange") Exchange exchange,@Qualifier("orderQueue") Queue queue){

            return BindingBuilder.bind(queue).to(exchange).with("order.*").noargs();
    }
}
