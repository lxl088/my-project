package cn.rabbitmq;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestRabbitmq {
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Test
    public void testProductQueue(){
        rabbitTemplate.convertAndSend("blog_exchange",
                "product.insert", "商品新增，routing key 为product.insert");
    }
    @Test
    public void testOrderQueue(){
        rabbitTemplate.convertAndSend("blog_exchange",
                "order.update", "订单更新，routing key 为order.update");
    }
}
