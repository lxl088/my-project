package cn.rabbitmq.listener;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class RabbitMQListener {
    @RabbitListener(queues = "product_queue")
    public void productListener(String message){
        System.out.println(message);
    }
    @RabbitListener(queues = "order_queue")
    public void orderListener(String message){
        System.out.println(message);
    }
}
