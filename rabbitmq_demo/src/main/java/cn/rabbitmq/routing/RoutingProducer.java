package cn.rabbitmq.routing;

import cn.rabbitmq.util.ConnectionUtil;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

/**
 * 路由模式
 * 交换机类型为：direct
 */
public class RoutingProducer {
    //交换机名称
    static final String DIRECT_EXCHANGE = "direct_exchange_demo1";
    //队列名称 更新商品队列名称
    static final String DIRECT_QUEUE_INSERT = "direct_queue_insert";
    //队列名称  新增商品队列名称
    static final String DIRECT_QUEUE_UPDATE = "direct_queue_update";

    public static void main(String[] args) {
        try {
            //创建连接
            Connection connection = ConnectionUtil.getConnection();
            // 创建频道
            Channel channel = connection.createChannel();
            /**
             * 声明交换机
             * 参数1：交换机名称
             * 参数2：交换机类型，fanout、topic、direct、headers
             */
            channel.exchangeDeclare(DIRECT_EXCHANGE, BuiltinExchangeType.DIRECT);
            // 声明（创建）队列
            /**
             * 参数1：队列名称
             * 参数2：是否定义持久化队列
             * 参数3：是否独占本次连接
             * 参数4：是否在不使用的时候自动删除队列
             * 参数5：队列其它参数
             */
            channel.queueDeclare(DIRECT_QUEUE_INSERT, true, false, false, null);
            channel.queueDeclare(DIRECT_QUEUE_UPDATE, true, false, false, null);
            //队列绑定交换机
            //新增服务 路由 key
            String routingKey_insert="insert";
            //更新服务 路由 key
            String routingKey_update="update";
            channel.queueBind(DIRECT_QUEUE_INSERT, DIRECT_EXCHANGE, routingKey_insert);
            channel.queueBind(DIRECT_QUEUE_UPDATE, DIRECT_EXCHANGE, routingKey_update);

            //一、发送消息 新增消息
                String message = "新增了商品。路由模式；routing key 为 "+routingKey_insert ;
                /**
                 * 参数1：交换机名称，如果没有指定则使用默认Default Exchage
                 * 参数2：路由key,简单模式可以传递队列名称
                 * 参数3：消息其它属性
                 * 参数4：消息内容
                 */
            channel.basicPublish(DIRECT_EXCHANGE,routingKey_insert,null,message.getBytes());
            System.out.println("消息已发送："+message);

            //二、发送消息 更新消息
            message = "更新了商品。路由模式；routing key 为 "+routingKey_update ;
            /**
             * 参数1：交换机名称，如果没有指定则使用默认Default Exchage
             * 参数2：路由key,简单模式可以传递队列名称
             * 参数3：消息其它属性
             * 参数4：消息内容
             */
            channel.basicPublish(DIRECT_EXCHANGE,routingKey_update,null,message.getBytes());
            System.out.println("消息已发送："+message);

            //关闭资源
            channel.close();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
