package cn.rabbitmq.topic;

import cn.rabbitmq.util.ConnectionUtil;
import com.rabbitmq.client.*;

import java.io.IOException;

/**
 * 通配模式 消息消费者 order
 */
public class TopicConsumer_order {
    //交换机名称
    static final String TOPIC_EXCHANGE = "topic_exchange_demo1";
    //队列名称
    static final String TOPIC_QUEUE_ORDER = "topic_queue_order";

    public static void main(String[] args) {
        try {
            Connection connection= ConnectionUtil.getConnection();

            //创建频道
            Channel channel=connection.createChannel();
            //声明交换机
            channel.exchangeDeclare(TOPIC_EXCHANGE, BuiltinExchangeType.TOPIC);
            /**
             * 声明 （创建）队列
             * 参数1：队列名称
             * 参数2：是否定义持久化队列
             * 参数3：是否独占本次连接
             * 参数4：是否在不使用的时候自动删除队列
             * 参数5：队列其它参数
             */
            channel.queueDeclare(TOPIC_QUEUE_ORDER, true, false, false, null);

            // 队列 绑定交换机
            channel.queueBind(TOPIC_QUEUE_ORDER, TOPIC_EXCHANGE, "order.update");
            channel.queueBind(TOPIC_QUEUE_ORDER, TOPIC_EXCHANGE, "order.insert");
            //创建消费者；并设置消息处理
            DefaultConsumer consumer = new DefaultConsumer(channel){
                @Override
                /**
                 * consumerTag 消息者标签，在channel.basicConsume时候可以指定
                 * envelope 消息包的内容，可从中获取消息id，消息routingkey，交换机，消息和重传标志
                 (收到消息失败后是否需要重新发送)
                 * properties 属性信息
                 * body 消息
                 */
                public void handleDelivery(String consumerTag, Envelope envelope,
                                           AMQP.BasicProperties properties, byte[] body) throws IOException {
                    //路由key
                    System.out.println("路由key为：" + envelope.getRoutingKey());
                    //交换机
                    System.out.println("交换机为：" + envelope.getExchange());
                    //消息id
                    System.out.println("消息id为：" + envelope.getDeliveryTag());
                    //收到的消息
                    System.out.println("订单接收到的消息为：" + new String(body, "utf-8"));

                }
            };
            /**
             * 参数1：队列名称
             * 参数2：是否自动确认，设置为true为表示消息接收到自动向mq回复接收到了，mq接收到回复会删除消
             息，设置为false则需要手动确认
             * 参数3：消息接收到后回调
             */
            channel.basicConsume(TOPIC_QUEUE_ORDER, true, consumer);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
