package cn.rabbitmq.topic;

import cn.rabbitmq.util.ConnectionUtil;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

/**
 * 通配模式
 * 交换机类型为：topic
 */
public class TopicProducer {
    //交换机名称
    static final String TOPIC_EXCHANGE = "topic_exchange_demo1";
    //产品 队列名称
    static final String TOPIC_QUEUE_PRODUCT = "topic_queue_product";
    //订单 队列名称
    static final String TOPIC_QUEUE_ORDER = "topic_queue_order";

    public static void main(String[] args) {
        try {
            //创建连接
            Connection connection = ConnectionUtil.getConnection();
            // 创建频道
            Channel channel = connection.createChannel();
            /**
             * 声明交换机
             * 参数1：交换机名称
             * 参数2：交换机类型，fanout、topic、direct、headers
             */
            channel.exchangeDeclare(TOPIC_EXCHANGE, BuiltinExchangeType.TOPIC);

            // 发送信息 新增订单
            String RoutingKey_order_insert="order.insert";
            String message = "新增了订单。Topic模式；routing key 为 "+RoutingKey_order_insert ;
            channel.basicPublish(TOPIC_EXCHANGE, RoutingKey_order_insert, null, message.getBytes());
            System.out.println("已发送消息：" + message);

            // 发送信息 更新订单
            String RoutingKey_order_update="order.update";
            message = "更新了订单。Topic模式；routing key 为 "+RoutingKey_order_update ;
            channel.basicPublish(TOPIC_EXCHANGE, RoutingKey_order_update, null, message.getBytes());
            System.out.println("已发送消息：" + message);

            // 发送信息 更新订单金额
            String RoutingKey_order_update_money="order.update.money";
            message = "更新了订单。Topic模式；routing key 为 "+RoutingKey_order_update_money ;
            channel.basicPublish(TOPIC_EXCHANGE, RoutingKey_order_update_money, null, message.getBytes());
            System.out.println("已发送消息：" + message);

            // 发送信息 删除商品
            String RoutingKey_product_delete="product.delete";
            message = "删除了商品。Topic模式；routing key 为 "+RoutingKey_product_delete ;
            channel.basicPublish(TOPIC_EXCHANGE, RoutingKey_product_delete, null, message.getBytes());
            System.out.println("已发送消息：" + message);

            // 发送信息 更新商品
            String RoutingKey_product_update="product.update";
            message = "更新了商品。Topic模式；routing key 为 "+RoutingKey_product_update ;
            channel.basicPublish(TOPIC_EXCHANGE, RoutingKey_product_update, null, message.getBytes());
            System.out.println("已发送消息：" + message);

            // 发送信息 更新商品的金额
            String RoutingKey_product_update_money="product.update.money";
            message = "更新了商品。Topic模式；routing key 为 "+RoutingKey_product_update_money ;
            channel.basicPublish(TOPIC_EXCHANGE, RoutingKey_product_update_money, null, message.getBytes());
            System.out.println("已发送消息：" + message);

            //关闭资源
            channel.close();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
