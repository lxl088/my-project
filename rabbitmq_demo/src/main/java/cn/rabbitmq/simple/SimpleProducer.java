package cn.rabbitmq.simple;

import cn.rabbitmq.util.ConnectionUtil;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

public class SimpleProducer {
    static final String QUEUE_NAME="simple_queue";

    public static void main(String[] args) {
        try {
            //创建一个连接
            Connection connection= ConnectionUtil.getConnection();
            //创建一个频道
            Channel channel=connection.createChannel();
            /**
             * 参数1：队列名称
             * 参数2：是否定义持久化队列 表示如果rabbitmq关闭，下次再启动时还有没有该队列 true有 false没有
             * 参数3：是否独占本次连接
             * 参数4：是否在不使用的时候自动删除队列
             * 参数5：队列其它参数
             */
            channel.queueDeclare(QUEUE_NAME,true,false,false,null);

            String message="班主任：小明，后天放假";
            /**
             * 参数1：交换机名称，如果没有指定则使用默认Default Exchange
             * 参数2：路由key,简单模式可以传递队列名称
             * 参数3：消息其它属性
             * 参数4：消息内容
             */
            channel.basicPublish("",QUEUE_NAME,null,message.getBytes());

            System.out.println("消息已发送："+message);

            //关闭连接
            channel.close();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
