package cn.rabbitmq.ps;

import cn.rabbitmq.util.ConnectionUtil;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

/**
 * 发布订阅模式消息提供者
 * 发布订阅模式交换机类型为：fanout
 */
public class PsProducer {
    //交换机名称
    static final String FANOUT_EXCHANGE="fanout_exchange";
    //订单微服务队列名称
    static final String FANOUT_QUEUE_ORDER="fanout_queue_order";
    //产品微服务队列名称
    static final String FANOUT_QUEUE_PRODUCT="fanout_queue_product";

    public static void main(String[] args) {
        try {
            //创建一个连接
            Connection connection= ConnectionUtil.getConnection();
            //创建一个频道
            Channel channel=connection.createChannel();

            /**
             * 声明交换机
             * 参数1：交换机名称
             * 参数2：交换机类型，fanout、topic、direct、headers
             */
            channel.exchangeDeclare(FANOUT_EXCHANGE, BuiltinExchangeType.FANOUT);

            /**
             * 声明（创建）队列（两个队列 FANOUT_QUEUE1、FANOUT_QUEUE2）
             * 参数1：队列名称
             * 参数2：是否定义持久化队列
             * 参数3：是否独占本次连接
             * 参数4：是否在不使用的时候自动删除队列
             * 参数5：队列其它参数
             */
            channel.queueDeclare(FANOUT_QUEUE_ORDER,true,false,false,null);
            channel.queueDeclare(FANOUT_QUEUE_PRODUCT,true,false,false,null);

            //将队列绑定到交换机
            channel.queueBind(FANOUT_QUEUE_PRODUCT,FANOUT_EXCHANGE,"");
            channel.queueBind(FANOUT_QUEUE_ORDER,FANOUT_EXCHANGE,"");

            //发送消息
                String message="小明购买了一个游戏本电脑";
                /**
                 * 参数1：交换机名称，如果没有指定则使用默认Default Exchage
                 * 参数2：路由key,简单模式可以传递队列名称
                 * 参数3：消息其它属性
                 * 参数4：消息内容
                 */
                channel.basicPublish(FANOUT_EXCHANGE,"",null,message.getBytes());

                System.out.println("消息已发送："+message);
            //关闭资源
            channel.close();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
