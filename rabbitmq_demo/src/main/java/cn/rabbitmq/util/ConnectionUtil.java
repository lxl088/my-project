package cn.rabbitmq.util;

import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class ConnectionUtil {

    //端口号
    private static int port = 5672;
    //ip地址
    private static String host = "127.0.0.1";
    //虚拟目录
    private static String virtualHost="/blog";
    //用户名（注意该用户必须有上面虚拟目录的对应的权限）
    private static String username="lxl001";
    //密码
    private static String password="123";
    public static Connection getConnection(){

        ConnectionFactory  connectionFactory=new ConnectionFactory();
        connectionFactory.setHost(host);
        connectionFactory.setPort(port);
        connectionFactory.setVirtualHost(virtualHost);
        connectionFactory.setUsername(username);
        connectionFactory.setPassword(password);
        Connection connection= null;
        try {
            connection = connectionFactory.newConnection();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
        return connection;
    }

}
