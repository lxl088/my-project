package cn.fastdfs;

import org.csource.common.MyException;
import org.csource.common.NameValuePair;
import org.csource.fastdfs.*;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;

@SpringBootTest
class FastDfsDemoApplicationTests {

    @Test
    void contextLoads() {
    }
    @Test
    void fileUpLoad() throws MyException, IOException {
        ClientGlobal.initByProperties("fastdfs-client.properties");
        TrackerClient trackerClient=new TrackerClient();
        TrackerServer trackerServer=trackerClient.getConnection();
        StorageServer storageServer=null;
        StorageClient1 storageClient1= new StorageClient1(trackerServer,storageServer);
        NameValuePair nameValuePair[]=null;
        //指定文件目录，以及文件后缀名
        String fileID=storageClient1.upload_file1("D:\\1.png","png",nameValuePair);
        System.out.println(fileID);
    }
    @Test
    void fileDownLoad() throws MyException, IOException {
        ClientGlobal.initByProperties("fastdfs-client.properties");
        TrackerClient trackerClient=new TrackerClient();
        TrackerServer trackerServer=trackerClient.getConnection();
        StorageServer storageServer=null;
        StorageClient1 storageClient1= new StorageClient1(trackerServer,storageServer);
        NameValuePair nameValuePair[]=null;
        byte[] bytes=storageClient1.download_file1("group1/M00/00/00/wKgBCGFO8riAQBgzAABAIwzlD3c515.png");
        FileOutputStream fos=new FileOutputStream(new File("D:\\10.png"));
        fos.write(bytes);
        fos.close();
    }
    @Test
    void hasTokenUrl() throws MyException, UnsupportedEncodingException, NoSuchAlgorithmException {
        String filename="M00/00/00/wKgBCGFO8riAQBgzAABAIwzlD3c515.png";
        String key="123456";
        String ip="http://192.168.1.8";
        String groupFilename="/group1/"+filename;
        int ts = (int) Instant.now().getEpochSecond();
        //根据文件名、时间、密钥生成token
        String token = ProtoCommon.getToken(filename, ts, key);
        StringBuilder sb = new StringBuilder();
        sb.append(ip).append(groupFilename).append("?token="+token).append("&ts="+ts);
        System.out.println(sb.toString());
    }


}
