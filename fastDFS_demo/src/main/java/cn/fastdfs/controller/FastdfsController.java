package cn.fastdfs.controller;

import cn.fastdfs.util.FastDFSUtils;
import org.csource.common.MyException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

@RestController
public class FastdfsController {
    @RequestMapping("/uploadFile")
    public Map<String,Object> uploadFile(MultipartFile file) throws MyException, UnsupportedEncodingException, NoSuchAlgorithmException {
        Map<String,Object> map=new HashMap<>();
        String file_url="";
        try{
            file_url= FastDFSUtils.upload(file);
        }catch (Exception e){
            map.put("url","");
            map.put("code","500");
            map.put("status","error");
            map.put("msg","上传出现故障，请稍后再试");
            return map;
        }
        if (file_url==null||file_url==""){

            map.put("url","");
            map.put("code","500");
            map.put("status","error");
            map.put("msg","上传出现失败");
            return map;
        }
        file_url=FastDFSUtils.getHasTokenUrl(file_url);
        map.put("url",file_url);
        map.put("code","200");
        map.put("status","success");
        map.put("msg","上传成功");
        System.out.println(file_url);
        return map;
    }
}
