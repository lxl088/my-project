package cn.fastdfs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FastDfsDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(FastDfsDemoApplication.class, args);
    }

}
